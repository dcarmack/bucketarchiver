import os
import sys
import subprocess
import threading
from utils import online_fsck

class repairWorker(threading.Thread):
    # class variable
    sem = threading.Semaphore(30)
    
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None):
        
        super(repairWorker, self).__init__(group=group, target=target, 
                            name=name)
        self.args = args
        self.kwargs = kwargs
        return
    
    def scan(self):
        outs, errs = online_fsck(self.kwargs['splunk_home'], cmd='scan', path=self.kwargs['path'])
        _status = errs.split('\n')[-3:-2][0]
        try: 
            _status.index('Corruption')
        except ValueError: 
            return True
        else:
            return False

    def repair(self):
        outs, errs = online_fsck(self.kwargs['splunk_home'], path=self.kwargs['path'], timeout=900)
        _status = errs.split('\n')[-3:-2][0]
        try: 
            _status.index('failReason')
        except ValueError: 
            return True
        else:
            return False
    
    def run(self):
        
        with repairWorker.sem:
            if self.scan():
                sys.stdout.write("Validated: {}\n".format(self.kwargs['path']))
            elif self.repair():
                sys.stdout.write("Repaired: {}\n".format(self.kwargs['path']))
            else:
                sys.stderr.write("Cannot repair {}\n".format(self.kwargs['path']))
        
    
def move(bucket, state):
    pass


if __name__ == '__main__':
    import argparse
    
    cli = argparse.ArgumentParser()
    cli.add_argument('splunk_home')
    cli.add_argument('index_path')
    args = cli.parse_args()
    
    workers = []
    
    for _root, _dirs, _files in os.walk(os.path.join(args.index_path), topdown=True):
        for bucket in [ x for x in _dirs if x.startswith('db_') or x.startswith('rb_') and not x.endswith('-tmp')]:
            path = os.path.normpath(os.path.join(_root, bucket))
            p = repairWorker(kwargs={"path": path, "splunk_home": args.splunk_home})
            workers.append(p)
            p.daemon = True
            p.start()
    
    for p in [x for x in workers if x.is_alive]:
        p.join()