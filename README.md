# Deprecated for archive #
use this for archive:
[https://bitbucket.org/splunk_cloud_team/bucket_archiver_v2/src/master/]

# bucket_archiver #

A utility to archive and restore Splunk buckets

## Why do I need this ##

Historically, migrating Splunk buckets from an indexer cluster has been problematic and cumbersome. There are various spl searches that
have been published to gather up the required lists, and scripts of varying utility to use the results of those searches to copy buckets
to remote locations. Problems have included lots of copy/paste and manual formatting of search results, having to copy lists of buckets
to each indexer with only the buckets it should upload, a long process to create md5 sums of the files and verification of those sums once
the data is restored, and finally -- they were all very slow because they did things in a single thread.

This utility can be used to identify and copy primary buckets from an indexer cluster to a remote storage location (not SmartStore) automatically
by querying the cluster master via rest and searching the local index locations for the actual buckets.

### Features ###

* supports python3.6+
* supports stand-alone, single site, and multisite indexers as sources and targets
* supports Windows and Linux indexers
* supports Splunk 6.6+
* md5 checksum verification on copy with retry
* identifies data already on the remote store (by md5 hash) and does not re-copy
* multiple threads (configurable)
* supports s3, snowball, and other s3-like destinations (yes, google cloud storage has an s3-like interface!)
* extendable to azure blob (future release)
* context aware help (e.g. bucket_archiver archive -h)
* verbose and debug logging

### How do I get set up? ###

* configure a python environment
  * install pip
  * install boto3
  * install requests
* pull the latest release branch
* cd bucketArchiver

```
python -m bucket_archiver -h

usage: bucket_archiver [-h] {archive,restore,config} ...

A utility to archive and restore Splunk buckets

  positional arguments:
    {archive,restore,config}

        Action
            archive             Copy buckets to remote location
            restore             Restore copied buckets to a Splunk Indexer
            config              Configure the Cluster Master and check connectivity

  optional arguments:
    -h, --help            show this help message and exit
```

### How do I run it ###

This app functions in 3 different capacities:

* archive (fully implemented)
* restore (fully implemented)
* configure (not implemented)

### archive ###

* currently supports s3, "s3-like", and snowball destinations.
* supports various bucket renaming options.
  * standard naming (db_latest_earliest_id_GUID)
  * "bid" naming (index~id~GUID)
  * path can include the source indexer GUID
* include or exclude internal indexes

### Who do I talk to ###

Michael F. Doyle, SplunkCloud PS  
mdoyle@splunk.com

