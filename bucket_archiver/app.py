#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-04 17:18:22
# @modify date 2020-05-04 19:01:29
# @desc [description]
#
import json
import logging
import multiprocessing
import sys
import time
from threading import Lock, Thread, current_thread

from bucket_archiver.common.threads import workerThread
from bucket_archiver.common.utils import (SplunkMigrationBucket, getMyGUID,
                                          getMyIndexes, getMyInstanceType,
                                          getMyPrimaryBuckets, getSessionKey)
try:
    from queue import Queue
except ImportError:
    from Queue import Queue

def archive(args, username, password):
    logger = logging.getLogger(__name__)
    logger.debug("args=\"{}\"".format(vars(args)))
    
    indexList = []
    bucketList = []

    try:
        
        indexList = getMyIndexes(args)
        
    except:
        msg = "Error getting index list"
        logger.critical(msg)
        sys.exit(msg)
        
    logger.debug(json.dumps({'timestamp': time.time(), 'indexList': indexList}))

    logger.info("Running in {} environment".format(args.cluster_type))

    if (args.instance_type == 'SLAVE'):
        
        logger.debug("Authentication to cluster master {}".format(args.clustermaster))
        
        try:
            args.cm_token = getSessionKey(args.clustermaster, username, password)
        except:
            msg = "Authentication Error"
            logger.exception(msg)
            sys.exit(msg)
           
        logger.info("Getting list of primary buckets from {}".format(args.clustermaster))
        
    else:
        
        logger.info("Getting list of all local buckets")

    try:
        
        bucketList = getMyPrimaryBuckets(args, indexList)
        
    except:
        
        msg = "Could not get list of primary buckets."
        logger.exception(msg)
        sys.exit(msg)
        

    args.worker_queue = Queue()
    args.transfer_queue = multiprocessing.JoinableQueue(10)
    
    args.thread_lock = Lock()
    
    for i in range(args.workers):
        worker = Thread(target=workerThread, name="migrateSnowballWorker-{}".format(i), args=(args, indexList,))
        worker.setDaemon(True)
        worker.start()

    if args.dest == 's3' or args.dest == 'snowball':
        from bucket_archiver.common.s3 import transferWorker
    else:
        raise SystemExit("Destination {} not supported".format(args.dest))
    
    if __name__ ==  '__main__':
        for i in range(15): 
            transfer_agent = multiprocessing.Process(target=transferWorker, args=(args,))
            transfer_agent.daemon = True
            transfer_agent.start()

    for item in bucketList:
        args.worker_queue.put(item)

    args.worker_queue.join()
    
    args.transfer_queue.join()
    
    
    logger.info("Done")

    return True

def restore(args):
    logger = logging.getLogger(__name__)
    logger.debug("hello, args={}".format(vars(args)))
    print("restore")

def config(args):
    logger = logging.getLogger(__name__)
    logger.debug("hello, args={}".format(vars(args)))
    print("config")


def run(args, username, password):
       
    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)5s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s')
    elif args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)5s [ %(name)s ] - %(message)s')
    else:
        logging.basicConfig(level=logging.WARNING, format='%(asctime)s %(levelname)5s [ %(name)s:%(funcName)s() ] message=%(message)s')

    logger = logging.getLogger(__name__)
    logger.debug("args=\"{}\"".format(vars(args)))

    # for any action run the following
    logger.info("Authenticating to local instance")
    
    try:
        
        args.my_token = getSessionKey("localhost:{}".format(args.local_port), username, password)
        
    except:
        
        msg = "Authentication Failure"
        logger.critical(msg)
        sys.exit(msg)

    logger.debug("Got Session Key {}".format(args.my_token))
        
    try:
        
        args.my_guid = getMyGUID(args)
        
    except:
        
        msg = "Problem getting my GUID"
        logger.critical(msg)
        sys.exit(msg)
    
    logger.debug("GUID={}".format(args.my_guid))

    try:
        
        args.instance_type, args.cluster_type = getMyInstanceType(args)
        
    except:
        
        msg = "Could not determine instance type."
        logger.critical(msg)
        sys.exit(msg)
     

    if args.action == "restore":
        restore(args)

    elif args.action == "archive":
        archive(args, username, password)

    elif args.action == "config":
        config(args)
