#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-08 07:40:51
# @modify date 2020-05-08 07:40:51
# @desc [description]
#
import argparse
import multiprocessing as mp

def getArgs():

    cliParser = argparse.ArgumentParser(prog="bucket_archiver", description="A utility to archive and restore Splunk buckets")
    # cliParser.add_argument('--local_port', type=int, help='The local REST port (default 8089)', default=8089)
    # cliParser.add_argument('--verbose', action='store_true')
    # cliParser.add_argument('--debug', action='store_false')
    # cliParser.add_argument('-w', '--workers', type=int, help='Number of worker threads (more is not always better)', default=2)
    # cliParser.add_argument('-t', '--test', help='Don\'t actually execute the copy operation', action='store_true')

    global_parser = argparse.ArgumentParser(add_help=False)
    global_parser.add_argument('--local_port', type=int, help='The local REST port (default 8089)', default=8089)
    global_parser.add_argument('--verbose', action='store_true')
    global_parser.add_argument('--debug', action='store_true')
    global_parser.add_argument('-n', '--my_no', type=int, help='My index number', default=None)
    global_parser.add_argument('-i', '--idx_count', type=int, help='Number of Indexers', default=None)
    global_parser.add_argument('-w', '--workers', type=int, help='Number of worker threads (more is not always better)', default=mp.cpu_count())
    global_parser.add_argument('-t', '--test', help='Don\'t actually execute the copy operation', action='store_true')
    global_parser.add_argument('--no_skip', help='Don\'t skip internal indexes', action='store_true')
    global_parser.add_argument('--clustermaster', help='(required for indexer clusters) uri for the cluster master <ip>|<dns>:port')
    global_parser.add_argument('--rename_buckets', help='If enabled will rename the buckets to the index~id~GUID format on copy', action='store_true')
    global_parser.add_argument('--add_guid_to_path', help='If enabled will add a prefix identifying the source of the bucket (for historical reasons)', action='store_true')



    s3_shared_parser = argparse.ArgumentParser(add_help=False)
    s3_required = s3_shared_parser.add_argument_group('S3 Requrired')
    s3_optional = s3_shared_parser.add_argument_group('S3 Optional')
    s3_required.add_argument('--access_key', help='aws S3 access key', default=None)
    s3_required.add_argument('--secret_key', help='aws S3 secret access key', default=None)
    s3_required.add_argument('--bucket_name', help='aws S3 bucket name', required=True)
    s3_optional.add_argument('--bucket_path', help='S3 object key prefix (aka path)', default='/')
    s3_optional.add_argument('--region_name', help='aws S3 region', default=None)
    s3_optional.add_argument('--endpoint_url', help='aws S3 endpoint url', default='https://s3.amazonaws.com')
    s3_optional.add_argument('--ca_bundle', help='s3 cert path')

    snowball_shared_parser = argparse.ArgumentParser(add_help=False)
    snowball_required = snowball_shared_parser.add_argument_group('Snowball Required')
    snowball_optional = snowball_shared_parser.add_argument_group('Snowball Optional')
    snowball_required.add_argument('--access_key', help='Snowball access key', required=True)
    snowball_required.add_argument('--secret_key', help='Snowball secret access key', required=True)
    snowball_required.add_argument('--bucket_name', help='Snowball bucket name', required=True)
    snowball_required.add_argument('--endpoint_url', help='Snowball endpoint url', required=True)
    snowball_required.add_argument('--ca_bundle', help='Snowball cert path', required=True)
    snowball_optional.add_argument('--bucket_path', help='Snowball object key prefix (aka path)', default='/')

    config_shared_parser = argparse.ArgumentParser(add_help=False)
    config_required = config_shared_parser.add_argument_group('Required')
    config_optional = config_shared_parser.add_argument_group('Optional')

    cmd_parser = cliParser.add_subparsers(help='Action', dest="action")

    archive_parser = cmd_parser.add_parser('archive', help="Copy buckets to remote location")

    dest_parser = archive_parser.add_subparsers(help="Destination Type", dest="dest")

    s3_dest_parser = dest_parser.add_parser('s3', help="Amazon S3 destination", parents=[s3_shared_parser, global_parser], conflict_handler='resolve')
    snowball_dest_parser = dest_parser.add_parser('snowball', help="Amazon Snowball destination", parents=[snowball_shared_parser, global_parser], conflict_handler='resolve')
    azure_dest_parser = dest_parser.add_parser('azure', help='not implemented', parents=[global_parser], conflict_handler='resolve' )
    gcp_dest_parser = dest_parser.add_parser('gcp', help='not implemented', parents=[global_parser], conflict_handler='resolve' )
    flat_dest_parser = dest_parser.add_parser('flat', help='not implemented', parents=[global_parser], conflict_handler='resolve' )


    # RESTORE
    restore_parser = cmd_parser.add_parser('restore', help="Restore copied buckets to a Splunk Indexer")
    
    src_parser = restore_parser.add_subparsers(help="Source Type", dest="src") 
    
    s3_src_parser = src_parser.add_parser('s3', help="Amazon S3 source", parents=[s3_shared_parser, global_parser], conflict_handler='resolve')
    
    
    
    
    config_parser = cmd_parser.add_parser('config', help="Configure the Cluster Master and check connectivity")

    return cliParser.parse_args()