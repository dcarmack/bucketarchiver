#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-04 18:51:49
# @modify date 2020-05-10 08:16:47
# @desc [description]
#
import json
import logging
import multiprocessing
import os
import posixpath
import sys
import time
from threading import Lock
import random

import boto3
from botocore.exceptions import ClientError
from botocore.config import Config

from bucket_archiver.common.utils import SplunkMigrationBucket, getFileChecksum

# python 2
try:
    FileExistsError
except NameError:
    FileExistsError = OSError

def getRemoteMetadata(s3, aws_bucket, aws_key):
    
    logger = logging.getLogger("getRemoteMetadata")
    logger.debug("hello")
    
    logger.debug("s3={}, aws_bucket={}, aws_key={}".format(s3, aws_bucket, aws_key))
    
    try:
        response = s3.head_object(Bucket=aws_bucket, Key=aws_key)
    
    except:
        response = None
        raise
    
    finally:
       return response


def updateRemoteMetadata():
    pass

def transferWorker(thread_lock, transfer_queue, args):
    logger = logging.getLogger('transferWorker')
    logger.debug("hello")
    logger.debug(args)
    
    config = Config(connect_timeout=120,
                    read_timeout=600,
                    retries = {"total_max_attempts": 20, "mode": "adaptive"})
    
    session = boto3.session.Session()
    
    if args.ca_bundle:
        try:
            s3 = session.client('s3', region_name=args.region_name, endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key, verify=args.ca_bundle, config=config)

        except Exception as e:
            logger.error("S3 Connection Error w/verify")
            logger.exception(e)
            logger.error("endpoint_url={}, aws_access_key_id={} , aws_secret_access_key={}, verify={}".format(args.endpoint_url, args.access_key, args.secret_key, args.ca_bundle))
            raise SystemExit
    else:         
        logger.info("Using default boto3 cert")
        try:
            s3 = session.client('s3', region_name=args.region_name, endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key, config=config, verify=True)
            
        except Exception as e:
                logger.error("S3 Connection Error")
                logger.exception(e)
                logger.error("endpoint_url={}, aws_access_key_id={} , aws_secret_access_key={}".format(args.endpoint_url, args.access_key, args.secret_key))
                raise SystemExit
    
    class ProgressPercentage(object):

        def __init__(self, filename):
            self.logger = logging.getLogger('ProgressPercentage')
            self._filename = filename
            self._size = float(os.path.getsize(filename))
            self._seen_so_far = 0
            self._lock = Lock()
    
        def __call__(self, bytes_amount):
            # To simplify, assume this is hooked up to a single filename
            with self._lock:
                self._seen_so_far += bytes_amount
                percentage = (self._seen_so_far / self._size) * 100
                self.logger.debug(
                    "%s  %s / %s  (%.2f%%)" % (
                        self._filename, self._seen_so_far, self._size,
                        percentage))
    
    while True:
    
        logger.debug("Getting item from queue")
        bucket = transfer_queue.get()
        
        if bucket == "__DONE__":
            transfer_queue.put("__DONE__")
            transfer_queue.task_done()
            logger.info("Thread done.")
            break # while
        
        # index = bucket.data['index']
        bucketPath = bucket.data['full_path']
        errors = 0
        args.verify_exising = False
        if not args.test:
            # aws s3 ls --profile snowballEdge --endpoint https://192.0.2.0:8443 --ca-bundle path/to/certificate
            # try:
            #     s3 = boto3.client('s3', endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key)
            # except Exception :
            #     logger.exception("S3 Connection Error")
            #     raise SystemExit("S3 Connection Error")
                
            for dir_, _, files in os.walk(bucketPath, topdown=True):
                for file in files:
                    retries = 0
                    bucket.data['results'] = []
                    results = dict()
                    while retries < 5:
                        rel_dir = os.path.relpath(dir_, bucketPath)
                        rel_file = os.path.normpath(os.path.join(rel_dir, file))
                
                        src_path = os.path.normpath(os.path.join(dir_, file))
                        
                        # try:
                        #     hashes = getFileChecksum(src_path)
                        # except FileNotFoundError:
                        #     logger.warning("File Not Found {}".format(src_path))
                        #     errors +=1
                        #     retries +=1
                        #     continue  
                        
                        splunk_bucket_name = os.path.split(bucketPath)[-1]
                        
                        if bucket.data['standalone'] == 1:
                                splunk_bucket_name = "{}_{}".format(splunk_bucket_name, args.my_guid)
                        
                        if args.rename_buckets:
                            if args.add_guid_to_path:
                                dest_path = posixpath.normpath("{}/{}/{}/{}/{}".format(args.bucket_path, args.my_guid, bucket.data['index'], bucket.title, rel_file.replace(os.path.sep, posixpath.sep)))
                            else:
                                dest_path = posixpath.normpath("{}/{}/{}/{}".format(args.bucket_path, bucket.data['index'], bucket.title, rel_file.replace(os.path.sep, posixpath.sep)))

                        else:
                            if args.add_guid_to_path:
                                dest_path = posixpath.normpath("{}/{}/{}/{}/{}".format(args.bucket_path, args.my_guid, bucket.data['index'], splunk_bucket_name, rel_file.replace(os.path.sep, posixpath.sep)))
                            else:
                                dest_path = posixpath.normpath("{}/{}/{}/{}".format(args.bucket_path, bucket.data['index'], splunk_bucket_name, rel_file.replace(os.path.sep, posixpath.sep)))

                        logger.debug("src_path {} dest_path {}".format(src_path, dest_path))
                        
                        results['title'] = bucket.title
                        results['normalized_name'] = splunk_bucket_name
                        results['id'] = bucket.data['id']
                        results['index'] = bucket.data['index']
                        results['source'] = src_path
                        results['dest'] = dest_path
                        # results['md5'] = hashes['hex_digest']
                        # results['etag_value'] = hashes['etag_value']
                        results['md5_verified'] = False
                        
                        try:
                            
                            response = s3.head_object(Bucket=args.bucket_name, Key=dest_path)
                            
                        except ClientError:
                            
                            pass
                        
                        else:
                            meta_data = response['Metadata']
                            
                            if args.verify_exising:
                                try:
                                    hashes = getFileChecksum(src_path)
                                    
                                except FileNotFoundError:
                                    logger.warning("File Not Found {}".format(src_path))
                                    errors +=1
                                    retries +=1
                                    continue
                                else:
                                    results['md5'] = hashes['hex_digest']
                                    results['etag_value'] = hashes['etag_value']
                                    
                                if response['ETag'].strip('"') == results['etag_value']:
                                    results['md5_verified'] = True
                                    logger.info("Bucket {} exists {} -- verified ".format(bucket.title, rel_file))
                                    break # retries
                            else:
                                logger.debug(meta_data)
                                # TODO check for md5_verified tag from remote
                                try:
                                    meta_data['md5_verified']
                                    
                                except KeyError:
                                    results['md5_verified'] = True
                                    logger.info("Bucket {} exists {} -- skipping".format(bucket.title, rel_file))
                                    break # retries

                                if meta_data['md5_verified'] == "True":
                                    results['md5_verified'] = True
                                    logger.info("Bucket {} exists {} -- skipping".format(bucket.title, rel_file))
                                    break # retries
                        
                        try:
                            hashes = getFileChecksum(src_path)
                            
                        except FileNotFoundError:
                            logger.warning("File Not Found {}".format(src_path))
                            errors +=1
                            retries +=1
                            continue # retries
                        else:
                            results['md5'] = hashes['hex_digest']
                            results['etag_value'] = hashes['etag_value']
                            
                        try:
                            s3.upload_file(src_path,
                                    args.bucket_name,
                                    dest_path,
                                    ExtraArgs={
                                        'Metadata': {
                                            'title': bucket.title,
                                            'id': bucket.data['id'], 
                                            'index': bucket.data['index'],
                                            'origin_name': bucket.data['bucket_root'], 
                                            'normalized_name': "{}/{}".format(splunk_bucket_name, rel_file.replace(os.path.sep, posixpath.sep)),
                                            'md5_verified': "False"
                                            }
                                        },
                                    Callback=ProgressPercentage(src_path))
                        except ClientError as e:
                            logger.exception("{}".format(e))
                            sys.exit("Problem uploading to s3. Check the permissions to the bucket.")
                            retries +=1
                            continue # retry
                        
                        except FileNotFoundError:
                            logger.warning("File {} moved -- retry".format(src_path))
                            # see if the file is in the cold path now
                            
                            retries +=1
                            continue # retry
                        
                        try:
                            response = s3.head_object(Bucket=args.bucket_name, Key=dest_path)
                            
                        except ClientError:
                            
                            # wait a bit and try again
                            time.sleep(2)
                            try:
                                
                                response = s3.head_object(Bucket=args.bucket_name, Key=dest_path)
                                
                            except ClientError:
                                
                                # send this back around
                                logger.warning("S3 client Error?")
                                response['ETag'] = "Client Error"
                                retries +=1
                                continue # retries
                                
                        if response['ETag'].strip('"') == results['etag_value']:
                            meta_data = response['Metadata']
                            logger.debug(meta_data)
                            # TODO update remote metadata tag md5_verified
                            
                            # DOES NOT WORK ON SNOWBALL
                            meta_data['md5_verified'] = "True"
                            
                            try:
                                s3.copy_object(Bucket=args.bucket_name, Key=dest_path,
                                            CopySource={"Bucket": args.bucket_name, "Key": dest_path},
                                            Metadata=meta_data,
                                            MetadataDirective='REPLACE')
                                
                            except ClientError:
                                pass
                            
                            results['md5_verified'] = True
                            results['retries'] = retries
                            logger.debug("{} MD5 Checksum verified {} == {}".format(results['source'], response['ETag'].strip('"'), results['etag_value']))
                            break # retries
                        
                        else:
                            
                            logger.debug(response)
                            logger.debug("{} MD5 Checksum error {} != {} -- will retry".format(results['source'], response['ETag'].strip('"'), results['etag_value']))
                            retries += 1
                    
                    if not results['md5_verified']:
                        
                        logger.critical("File failed to upload completely - {}".format(src_path))
                        errors += 1
    
                    with thread_lock:
                        
                        with open("bucketstatus.json", "a") as f:
                            
                            f.write(json.dumps(results, sort_keys=True, separators=(',', ':')))
                            f.write("\n")
                
            logger.info("Bucket {} transfered to s3 with {} errors".format(bucket.title, errors))
        
        transfer_queue.task_done()
        
    return True

def getMyRestoreBuckets(args):
    logger = logging.getLogger('getRestoreBuckets')
    logger.debug("hello") 
    
    bucketList = []
    
    try:
        s3 = boto3.client('s3', region_name=args.region_name, endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key, verify=True)
    except Exception:
        logger.exception("S3 Connection Error")
        raise SystemExit("S3 Connection Error")
    
    params = {
        'Bucket': args.bucket_name,
        'Prefix': args.bucket_path
        }
    
    paginator = s3.get_paginator('list_objects')

    pages = paginator.paginate(**params)
    
    for page in pages:
        try:
            contents = page['Contents']
            
        except KeyError:
            logger.info("No more matching items. Search complete")
            break
        
        for item in contents:
            resp = s3.head_object(Bucket=args.bucket_name, Key=item['Key'])
            
            if (int(resp['Metadata']['id']) % args.idx_count) != args.my_no:
                continue
            
            else:                   
                meta = dict()
                try:
                    meta = resp['Metadata']
                except KeyError:
                    msg = "S3 buckets not archived properly. Did you use this utility to create the archive? Use the AWS CLI to copy these buckets."
                    logger.critical(msg)
                    raise SystemExit(msg)

                logger.debug(meta)
                
                bucket = SplunkMigrationBucket(meta['title'], key=item['Key'], ETag=item['ETag'].strip('"'))
                
                logger.debug('Title: "{}", Key: "{}", ETag: "{}"'.format(bucket.title, bucket.data['key'], bucket.data['ETag']))
                
                try:
                    for k, v in meta.items():
                        logger.debug("adding key={} value={}".format(k, v))
                        bucket.data[k] = v
                        
                except ValueError:
                    print("Error: {}".format(meta))

                logger.debug("Adding item {} to bucketList".format(bucket.title))
                bucketList.append(bucket)
                
    logger.debug("Returning BucketList {}".format(bucketList))
    return bucketList

def getMyRestoreBuckets1(args):
    logger = logging.getLogger('getRestoreBuckets')
    logger.debug("hello") 
    
    
    try:
        s3 = boto3.client('s3', region_name=args.region_name, endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key, verify=True)
    except Exception:
        logger.exception("S3 Connection Error")
        raise SystemExit("S3 Connection Error")
    
    for index in args.indexList:

        
        params = {
            'Bucket': args.bucket_name,
            'Prefix': "{}/{}".format(args.bucket_path, index['title']),
            'PaginationConfig':{'PageSize': 1000}
            }
        
        
        # TODO add retries
        retries = 0
        while True:
            try:
                paginator = s3.get_paginator('list_objects')
                
            except Exception as e:
                retries += 1
                if retries < 20:
                    time.sleep(retries)
                    logger.warning("AWS connection problems {}".format(e.args))
                    continue
                else:
                    logger.error("AWS connection failure {}".format(e.args))
                    raise SystemExit(e)
                    
            else:
                break # continue
            

        pages = paginator.paginate(**params)
        
        for page in pages:
            try:
                contents = page['Contents']
                
            except KeyError:
                logger.info("No more matching items. Search complete")
                break
            
            for item in contents:
                
                if (int(item['Key'].split("~")[1]) % args.idx_count) != args.my_no:
                    continue
                
                else: 
                    resp = s3.head_object(Bucket=args.bucket_name, Key=item['Key'])
                            
                    raw_meta = dict()
                    try:
                        raw_meta = resp['Metadata']
                    except KeyError:
                        msg = "S3 buckets not archived properly. Did you use this utility to create the archive? Use the AWS CLI to copy these buckets."
                        logger.critical(msg)
                        raise SystemExit(msg)

                    logger.debug(raw_meta)
                    
                    # added for minio compatibility
                    meta = dict()
                    for k, v in raw_meta.items():
                        meta[k.lower()] = v
                    
                    bucket = SplunkMigrationBucket(meta['title'], key=item['Key'], ETag=item['ETag'].strip('"'))
                    
                    # try:
                    #     bucket = SplunkMigrationBucket(meta['title'], key=item['Key'], ETag=item['ETag'].strip('"'))
                        
                    # added for minio compatibility
                    # except KeyError:
                    #     bucket = SplunkMigrationBucket(meta['Title'], key=item['Key'], ETag=item['ETag'].strip('"'))
                    
                    logger.debug('Title: "{}", Key: "{}", ETag: "{}"'.format(bucket.title, bucket.data['key'], bucket.data['ETag']))
                    
                    try:
                        for k, v in meta.items():
                            logger.debug("adding key={} value={}".format(k, v))
                            bucket.data[k] = v
                            
                    except ValueError:
                        print("Error: {}".format(meta))

                    logger.debug("Adding item {} to bucketList".format(bucket.title))
                    yield bucket
                    
                
def downloadWorker(thread_lock, download_queue, args):
    logger = logging.getLogger(multiprocessing.current_process().name)
    logger.debug("hello") 
    
    # aws s3 ls --profile snowballEdge --endpoint https://192.0.2.0:8443 --ca-bundle path/to/certificate
    try:
        s3 = boto3.client('s3', region_name=args.region_name, endpoint_url=args.endpoint_url, aws_access_key_id=args.access_key , aws_secret_access_key=args.secret_key, verify=True)
    except Exception :
        logger.exception("S3 Connection Error")
        raise SystemExit("S3 Connection Error")
    
    while True:
    
        bucket = download_queue.get()
        
        index = bucket.data['index']
        normalized_name = bucket.data['normalized_name']
        
        src_path = bucket.data['key']
        
        errors = 0
        results = dict()
        
        if True:
            # download the file from s3 to local index homePath
            try:
                index = next(item for item in args.indexList if item['title'] == bucket.data['index'])
            
            except StopIteration:
                logger.warning("Index {} is not present on this system. If you want this data, create the index and run this utility again.".format(bucket.data['index']))
                logger.debug("Skipping {}".format(bucket.title))
                download_queue.task_done()
                continue
            
            # dest_file = os.path.normpath(os.path.join(index['homePath'], normalized_name.replace("db_", "rb_")))
            # dest_file2 = os.path.normpath(os.path.join(index['coldPath'], normalized_name.replace("db_", "rb_")))
            dest_file = os.path.normpath(os.path.join(index['thawedPath'], normalized_name.replace("db_", "rb_")))
            
            logger.debug(dest_file)
            
            # TODO: check to see if the data is already present and the checksum matches
            if os.path.isfile(dest_file):
                # logger.debug("file exists, validating checksum")
                # if getFileChecksum(dest_file) == bucket.data['ETag'].strip('"'):
                #     logger.info("Same file exists -- skipping")
                logger.info("########### file exists {} ############# skipping".format(dest_file))
                download_queue.task_done()
                continue
            
            # create the dest dir
            dest_dir = os.path.normpath(os.path.dirname(dest_file))
            
            try:
                os.makedirs(dest_dir)
            except FileExistsError:
                logger.debug("Directory already exists - no worries")
                pass
            except Exception as e:
                logger.warning("Something went wrong creating dest path {}".format(dest_dir))
                raise SystemExit(e)
            

            
            
            
            
            results['title'] = bucket.title
            results['normalized_name'] = normalized_name
            results['id'] = bucket.data['id']
            results['index'] = bucket.data['index']
            results['source'] = src_path
            results['dest'] = dest_file
            # results['md5'] = hashes['hex_digest']
            # results['etag_value'] = hashes['etag_value']
            results['md5_verified'] = False
            retries = 0
            
            
            
            if not args.test:
                logger.info("Copying src_path {} to dest_path {}".format(src_path, dest_file))
                while retries < 20:
                    try:
                        s3.download_file(
                                args.bucket_name,
                                src_path,
                                dest_file
                        )
        #                             Callback=None)
                                # Callback=ProgressPercentage(src_path))
                                
                    except ClientError as e:
                        logger.exception("{}".format(e))
                        retries += 1
                        time.sleep(retries)
                        continue
                    else:
                        break # retries
            else:
                logger.info("###### TEST MODE ######## Copying src_path {} to dest_path {}".format(src_path, dest_file))
                time.sleep(random.uniform(0.1,45))
            
            if retries >= 20:
                logger.error()
                errors +=1
                results['status'] = "failed"
            else:
                results['status'] = "success"
            # TODO: verify checksum
            
    
        with thread_lock:
            
            with open("download-status.json", "a") as f:
                
                f.write(json.dumps(results, sort_keys=True, separators=(',', ':')))
                f.write("\n")
                
        logger.info("File {} transfered from s3 with {} errors".format(dest_file, errors))
        
        download_queue.task_done()
        
    return True
