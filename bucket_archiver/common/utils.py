#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-04 13:42:46
# @modify date 2020-05-04 13:43:25
# @desc [description]
#
import base64
import csv
import hashlib
import logging
import sys
from xml.dom import minidom

import requests

from bucket_archiver.splunklib.data import load

# python 2
try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError

def splunk_rest(host_uri, endpoint, headers=None, params=None, data=None, timeout=5, verify=True, stream=False):
    logger = logging.getLogger("splunk_rest")
    logger.debug("hello")

    try:

        resp = requests.get("{}/{}".format(host_uri, endpoint), headers=headers,
                            params=params, verify=verify, data=data, timeout=timeout, stream=stream)

    except requests.exceptions.RequestException as e:
        raise e

    return resp

class SplunkMigrationBucket(object):
    '''
    classdocs
    '''

    def update(self, **kwargs):
        for key, value in kwargs.items():
            self.data[key] = value
            
    def __init__(self, title, **kwargs):
        '''
        Constructor
        '''
        self.title = title
        self.data = dict()
        
        for key, value in kwargs.items():
            self.data[key] = value

def getPeers(args):
    
    logger = logging.getLogger('getPeers')
    logger.debug("hello")
    
    headers = {'Authorization': "Splunk {}".format(args.cm_token)}
    params = {'count': 0}
    
    try:
        resp = splunk_rest(args.clustermaster, "services/cluster/master/peers", headers=headers, params=params, verify=False, timeout=60)
        
    except:
        logger.exception("Error getting list of peers")
        raise
        
    atom = load(resp.text)
    
    restart_times = dict()
    
    for entry in atom['feed']['entry']:
        # peer = dict()
        # peer['title'] = entry['title']
        host, _port = entry['content']['host_port_pair'].split(":")
        
        try:
            resp = splunk_rest("https://{}:8089".format(host), "services/server/info", headers=headers, verify=False, timeout=60)
        
        except:
            logger.exception("Error getting list of peers")
            raise

        peer_info = load(resp.text)
        
        guid = peer_info['feed']['entry']['content']['guid']
        startup_time = peer_info['feed']['entry']['content']['startup_time']
        restart_times[guid] = startup_time

    # a dictionary with guid as the key and startup_time as the value
    return restart_times
    

def getFileChecksum(file, block_size=8 * 1024 * 1024):

    logger = logging.getLogger('getFileChecksum')
    logger.debug("hello")
    logger.debug(file)
    
    checksum = hashlib.md5()
    etags = []
    
    result = dict({'file': file, 'hex_digest': None, 'base64_digest': None})
    
    with open(file, "rb") as f:
        buf = f.read(block_size)
        while len(buf) > 0:
            checksum.update(buf)
            etags.append(hashlib.md5(buf))
            buf = f.read(block_size)
            
    result['hex_digest'] = checksum.hexdigest()
    result['base64_digest'] = base64.b64encode(checksum.digest()).decode('utf-8')
    
    if len(etags) < 1:
        result['etag_value'] = hashlib.md5().hexdigest()
    elif len(etags) == 1:
        result['etag_value'] = etags[0].hexdigest()
    else:
        result['etag_value'] = "{}-{}".format(hashlib.md5(b''.join(x.digest() for x in etags)).hexdigest(), len(etags))
        
    result['etag_block_size'] = block_size
    
    logger.debug("Computed checksum for bucket {} is hex: {} base64: {} etag: {} etag_block_size: {}".format(
                    result['file'],
                    result['hex_digest'],
                    result['base64_digest'],
                    result['etag_value'],
                    result['etag_block_size']))

    return result

def getSessionKey(uri, username, password):
    logger = logging.getLogger(__name__)
    logger.debug({'uri': uri, 'username': username})

    userdata = {'username': username, 'password': password}
    try:

        resp = splunk_rest(uri, "services/auth/login", data=userdata, verify=False)

    except requests.exceptions.RequestException:

        msg = "Connection to {} refused. Verify the REST port.".format(
            uri)
        logger.exception(Exception)
        raise SystemExit(msg)

    atom = load(resp.text)
    
    return atom['response']['sessionKey']


def getMyGUID(args):
    logger = logging.getLogger("getMyGUID")
    logger.debug("hello")

    headers = {'Authorization': "Splunk {}".format(args.my_token)}

    try:

        resp = splunk_rest("https://localhost:{}".format(args.local_port),
                           "services/server/info",
                           headers=headers,
                           verify=False,
                           timeout=1,
                           stream=False)

    except:

        logger.exception("Error getting GUID")
        raise

    else:
        atom = load(resp.text)
        return atom['feed']['entry']['content']['guid']


def getMyIndexes(args):
    logger = logging.getLogger("getMyIndexes")
    logger.debug("hello")

    indexTemplate={'title': None, 'coldPath': None, 'homePath': None, 'thawedPath': None, 'isInternal': None}

    headers = {'Authorization': "Splunk {}".format(args.my_token)}

    params = {'datatype': 'all', 'count': 0}

    try:

        resp = splunk_rest("https://localhost:{}".format(args.local_port),
                           "services/data/indexes",
                           headers=headers,
                           params=params,
                           verify=False,
                           timeout=60,
                           stream=False)

    except:

        logger.exception("Error getting index list")
        raise

    indexList = []

    atom = load(resp.text)

    for entry in atom['feed']['entry']:
        index = dict(indexTemplate)
        index['title'] = entry['title']
        index['coldPath'] = entry['content']['coldPath_expanded']
        index['homePath'] = entry['content']['homePath_expanded']
        index['thawedPath'] = entry['content']['thawedPath_expanded']
        index['isInternal'] = entry['content']['isInternal']

        indexList.append(index)

    return indexList


def getMyInstanceType(args):
    logger = logging.getLogger("getMyInstanceType")
    logger.debug("hello")

    headers = {'Authorization': "Splunk {}".format(args.my_token)}
    try:
        resp = splunk_rest("https://localhost:{}".format(args.local_port),
                           "services/cluster/master/info",
                           headers=headers,
                           verify=False,
                           timeout=60
                           )

    except:
        logger.exception("REST Error getting instance type")
        raise

    atom = load(resp.text)

    try:
        if atom['feed']:
            logger.debug("I AM THE CLUSTER MASTER")
            instance_type = "MASTER"

    except KeyError:
        try:
            resp = splunk_rest("https://localhost:{}".format(args.local_port),
                               "services/cluster/slave/info",
                               headers=headers,
                               verify=False,
                               timeout=60
                               )

        except:
            logger.exception("REST Error getting instance type")
            raise

        atom = load(resp.text)

        try:
            if atom['feed']:
                logger.debug("I AM A SLAVE INDEXER")
                instance_type = "SLAVE"
                
                if atom['feed']['entry']['content']['site'].startswith("site"):
                    cluster_type = 'multisite'
                    
                else:
                    cluster_type = 'clustered'

        except KeyError:
            logger.debug("I AM STANDALONE")
            instance_type = "STANDALONE"
            cluster_type = 'standalone'

    return instance_type, cluster_type


def getMyPrimaryBuckets(args):
    epoch_filter = '1628107863'
    logger = logging.getLogger("getMyPrimaryBuckets")
    logger.debug("hello")
    
    if args.cluster_type == "standalone":
        bucketList = []
        for item in args.indexList:
            logger.debug(item)
            try:
                with open("{}/.bucketManifest".format(item['homePath']), "r") as f:
                    linereader = csv.reader(f)
                    
                    for line in linereader:
                        bucket = None
                        
                        if line[0] != "id" and not line[1].startswith("hot"):
                            logger.debug(line)
                            bucket_parts = line[1].split("_")
                            lt = int(bucket_parts[1])
                            et = int(bucket_parts[2])
                            bucket = SplunkMigrationBucket(line[0])
                            if lt >= int(epoch_filter) and et <= int(epoch_filter):
                                index, id, guid = bucket.title.split("~")
                                bucket.update(index=index, id=id, guid=guid, standalone=1, retries=0)
                                bucketList.append(bucket)
                            else:
                                msg = '{} is outside of epoch filter {}, skipping bucket {}'.format(line[1], epoch_filter, bucket)
                                logger.warning(msg)

                        else:
                            continue
                        
            except FileNotFoundError:
                logger.warning("No .bucketManifest found for index {}".format(item['title']))
                continue
    else:

        headers = {'Authorization': "Splunk {}".format(args.my_token)}
        
        # params = dict()
        params = [
                ('filter', 'status=Complete'),
                ('count', 0),
                ('offset', 0),
                ('sort_key', 'title'),
                ('sort_dir', 'desc'),
                ('search', 'frozen=0'),
                ('f', 'title'),
                ('f', 'standalone')
        ]
        
        if args.cluster_type == 'multisite':
            search = 'generations.0=*1 (latest_time>={} AND earliest_time<={})'.format(epoch_filter, epoch_filter)
            params.append(('search', search))
            
        elif args.cluster_type == 'clustered':
            msg = "Not tested on single site cluster, exiting... {}".format(args.cluster_type)
            logger.exception(msg)
            raise Exception
            
        else:
            msg = "Unknown instance type {}".format(args.cluster_type)
            logger.exception(msg)
            raise Exception
        
        try:
            resp = splunk_rest("https://localhost:{}".format(args.local_port), "/services/cluster/peer/buckets", headers=headers, params=params, verify=False, timeout=3600)
            
        except:
            logger.exception("Error getting primary buckets")
            raise
        
        else:
            atom = load(resp.text)
            bucketList = []
            # for item in atom['feed']['entry']['content']:
            for item in atom['feed']['entry']:
                bucket = SplunkMigrationBucket(item['title'])
                index, id, guid = bucket.title.split("~")
                bucket.update(index=index, id=id, guid=guid, standalone=item['content']['standalone'], retries=0)
                bucketList.append(bucket)
            
    if not args.no_skip:
        bucketList = list(filter(lambda x: not x.data['index'].startswith("_"), bucketList))
            
    return bucketList

def getIndexerPosition(args):
    logger = logging.getLogger("getIndexerPosition")
    logger.debug("hello")
    
    if args.cluster_type == 'standalone':
        logger.debug("Standalone Indexer - returning 0, 1")
        my_no = 0
        peer_count = 1
        
    else:
        logger.debug("Clustered Indexer -- getting count and position")
        
        headers = {'Authorization': "Splunk {}".format(args.cm_token)}
        params = {'count': 0}
        
        try:
            resp = splunk_rest(args.clustermaster, "services/cluster/master/peers", headers=headers, params=params, verify=False, timeout=60)
            
        except:
            logger.exception("Error getting list of peers from {}".format(args.clustermaster))
            raise SystemExit()
        
        atom = load(resp.text)
        
        peer_list =[]
        
        for item in atom['feed']['entry']:
            peer_list.append(item['title'])
            
        # peer_list = [
        #     "001A2F98-AD46-492B-960C-8F4D6CD32C84", 
        #     "0FF6D740-6477-4C97-9949-58FD7629CD5D", 
        #     "31F1D95F-EF71-416B-A728-D351A1F39E50", 
        #     "538006E9-422A-4572-B94B-8EBC077BDE21", 
        #     "5CEBF07A-5311-4CB6-9699-0BCB2984647A", 
        #     "AC63A5B4-30F9-4BBB-817E-C555A8A8D4EC", 
        #     "B4C72279-EEB9-43E7-9000-93521AC8F2B9"
        #     ]
        
        peer_list.sort()
        try:
            my_no = peer_list.index(args.my_guid)
        except ValueError:
            print("Something is horribly wrong")
            raise SystemExit("Something is horribly wrong")
        peer_count = len(peer_list)
    
    logger.info("I am INDEXER {} of {}".format(my_no, peer_count))
    return my_no, peer_count




def getServerInfo(args):
    logger = logging.getLogger("getServerInfo")
    logger.debug("hello")
    
    headers = {'Authorization': "Splunk {}".format(args.my_token)}
    try:
        resp = splunk_rest("https://localhost:{}".format(args.local_port),
                           "services/server/info",
                           headers=headers,
                           verify=False,
                           timeout=60
                           )

    except:
        logger.exception("REST Error getting server info")
        raise

    atom = load(resp.text)
    
    guid = atom['feed']['entry']['content']['guid']
    startup_time = atom['feed']['entry']['content']['startup_time']
    server_name = atom['feed']['entry']['content']['serverName']  
          
    try:
        master = atom['feed']['entry']['content']['server_roles'].index("cluster_master")
        
    except ValueError:
        logger.debug("NOT master")
        master = None
        try:
            slave = atom['feed']['entry']['content']['server_roles'].index("cluster_slave")
            
        except ValueError:
            logger.debug("NOT slave")
            slave = None
            try:
                standalone = atom['feed']['entry']['content']['server_roles'].index("indexer")
                
            except ValueError:
                logger.debug("NOT standalone")
                standalone = None
                msg = "Invalid instance type {}".format(atom['feed']['entry']['content']['server_roles'])
                logger.critical(msg)
                raise SystemExit(msg)
    
    if slave is not None:  
        try:
            resp = splunk_rest("https://localhost:{}".format(args.local_port),
                                # "services/cluster/slave/info",
                                "/servicesNS/nobody/search/configs/conf-server/clustering",
                                headers=headers,
                                verify=False,
                                timeout=60
                                )
        except:
            msg = "REST Error getting slave cluster type"
            logger.exception(msg)
            raise SystemExit(msg)

        atom = load(resp.text)

        if atom['feed']['entry']['content']['multisite'] == "1":
            cluster_type = 'multisite'
        else:
            cluster_type = 'clustered'
        
        master_uri = atom['feed']['entry']['content']['master_uri']
        
        return (
            'SLAVE',
            guid,
            master_uri,
            cluster_type,
            server_name,
            startup_time
        )
    elif standalone is not None:
        return (
            'STANDALONE',
            guid,
            None,
            'standalone',
            server_name,
            startup_time
        )
    elif master is not None:
        return (
            'MASTER',
            guid,
            None,
           'master',
            server_name,
            startup_time
        )
    else:
        logger.debug("master={} slave={} standalone={}".format(master,slave,standalone))
        logger.debug(atom['feed']['entry']['content']['server_roles'])
        msg = "Something is horribly wrong"
        logger.critical(msg)
        raise SystemExit(msg)
    