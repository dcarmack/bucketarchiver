import os
import gzip
import subprocess 

REDACT_KEYS = ["password"]

def redact_values(in_dict, redact=None):
    
    if isinstance(redact, list):
        REDACT_KEYS.extend(redact)
    elif isinstance(redact, str):
        REDACT_KEYS.append(redact)
    elif redact is None:
        pass
    else:
        raise ValueError
    
    return {k: (lambda x: in_dict[x] if x not in REDACT_KEYS else "--REDACTED--")(k) for k in in_dict.keys()}

def get_origin_site(journal):
    """Returns the origin site of the provided journal file

    Args:
        journal (str): path to the journal.gz

    Returns:
        str: name of origin site (e.g. site2)
    """
    with gzip.open(journal, "rb") as f:
        data = f.read(32)
        
    if (lambda x: True if x == 2 else False)(int(data[20])):
        site = data[21:20+int(data[19])].decode('utf-8')
        return site
    else:
        return ""
    
def get_index_earliest(journal):
    """Returns the epoch time of the earliest _index_time for the provided journal file

    Args:
        journal (str): path to the journal.gz
        
    Returns:
        int: epoch time in seconds
    """
    with open(journal, "rb") as f:
        data = f.read(10)
    
    return int.from_bytes(data[4:8], byteorder='little')

def get_index_latest(path):
    pass

def get_index_earliest_latest(path):
    
    _metafiles = ["Hosts.data", "Sources.data", "SourceTypes.data"]
    _journal = os.path.join(path, 'rawdata', 'journal.gz')
    
    _index_earliest = get_index_earliest(_journal)
    print(_index_earliest)
    for _file in [ os.path.join(path, x) for x in _metafiles]:
        with open(_file, "r") as f:
            _latest = f.readline().split()[5]
            print(_latest)
    

def online_fsck(splunk_home, cmd='repair', path="", timeout=600):
    
    # repair the bucket
    cmd = [os.path.join(splunk_home, "bin", "splunk"), "cmd", "splunkd", "fsck", cmd, "--one-bucket", "--bucket-path={}".format(path)]
    _proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
    try:
        outs, errs = _proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired as e:
        _proc.kill()
        outs, errs = _proc.communicate()
        raise e
    return (outs, errs)
    
def fix_metadata(splunk_home, path=""):
    
    cmd = [os.path.join(splunk_home, "bin", "splunk"), "cmd", "splunkd", "recover-metadata", str(path), "--fixup-bucket-metadata-after-delete"]
    _proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    try:
        outs, errs = _proc.communicate(timeout=2)
        
    except subprocess.TimeoutExpired as e:
        _proc.kill()
        outs, errs = _proc.communicate()
        raise e

def create_bucket_info(splunk_home, path=""):
    
    try: fix_metadata(splunk_home, path=path)
    except subprocess.TimeoutExpired:
        try: online_fsck(splunk_home, cmd='repair', path=path)
        except subprocess.TimeoutExpired: return False
    
    return get_index_earliest(os.path.join(path, "rawdata", "journal.gz"))
        
